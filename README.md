>-----
>
> # Movie ticket booking Application #
>
>> by _WASAN SRIYARAN 6110406198_
>
>-----
>
> ##### This project is part of 01418211 Software Construction 1st semester in 2019 #####
> 
> How to use


> 1. Login with your username and password

> 2. Select Movie

> 3. Select Date, Showtime and Theatre what you want and click on "Booking Now" button. 
>When current time is after Showtime. It can't select that showtime.

> 4. Its will show seats in theatre you selected.

>   *  Green Seat --> You can select this
>   *  Blue Seat  --> This seat is selected
>   *  Red Seat   --> Reserved Seat (you can't select)
>
>>When you click on green seat its turn to blue seat, if click again its will turn to green seat (unselect) If you finished, Click on "Booking" button
>
>  5. If all booking is correct, click "Confirm booking" to finish booking then program will ask to next process include 
>
>   *  "Back to booking"
>   *  "Exit program"
>   *  "Log out"
>   
>   If you want to back to booking, program will refresh page and show previous selected seat (turn to red seat)




