import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;

public class MainProgram extends Application {

    public static void main(String[] args) {
        // create "data" directory if it not exist
        final String FS = File.separator;
        String currentDir = System.getProperty("user.dir");
        File dataFolder = new File(currentDir + FS + "data");
        if (!dataFolder.exists()) dataFolder.mkdir();
        launch(args);
    }


    @Override
    public void start(Stage primaryStage) throws IOException {



        Parent root = FXMLLoader.load(getClass().getResource("/Views/LoginPage.fxml"));
        root.getStylesheets().add(getClass().getResource("/StyleSheet/LoginPageStyle.css").toExternalForm());
        primaryStage.setTitle("Welcome to Oscar Cinema");
        primaryStage.setScene(new Scene(root, 800, 600));
        primaryStage.setResizable(false);
        primaryStage.show();

    }
}
