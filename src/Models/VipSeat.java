package Models;

public class VipSeat extends Seat{
    private double price;
    public VipSeat(String position) {
        super(position);
        this.price = 250;
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public void setPrice(double price) {
        this.price = price;
    }
}
