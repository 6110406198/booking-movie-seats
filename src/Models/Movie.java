package Models;

import javafx.scene.image.Image;

import java.util.ArrayList;
import java.util.List;

public class Movie {
    private String name;
    private String pathCover;
    private Image cover;
    private String description;
    private List<ShowTime> showTimeList;

    public Movie(String name, Image cover) {
        this.name = name;
        this.showTimeList = new ArrayList<>();
    }

    public Movie(String name, String cover) {
        this.name = name;
        this.pathCover = cover;
        setCover(cover);
    }

    public void setCover(Image cover) {
        this.cover = cover;
    }

    public void setCover(String path) {
        setCover(new Image(path));
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public Image getCover() {
        return cover;
    }

    public String getDescription() {
        return description;
    }

    public String getPathCover(){
        return pathCover;
    }

    @Override
    public String toString() {
        return name + "," + pathCover;
    }
}
