package Models;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Theatre {
    private String name;
    private Map<String, Seat> seatMap;

    public Theatre(String name ) {
        this.name = name;
        seatMap = new HashMap<>();
    }

    public void addAllSeat(List<Seat> seats){
        for (Seat seat: seats){
            addSeat(seat);
        }

    }

    public void addSeat(Seat seat){
        seatMap.put(seat.getPosition(), seat);
    }

    public String getName() {
        return name;
    }

    public Map<String, Seat> getSeatMap() {
        return seatMap;
    }
}
