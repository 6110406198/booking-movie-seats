package Models;

import javafx.scene.image.Image;

import java.util.HashMap;
import java.util.Map;

public class NormalSeat extends Seat{
    private double price;

    public NormalSeat(String position) {
        super(position);
        this.price = 150.0;
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public void setPrice(double price) {
        this.price = price;
    }

}
