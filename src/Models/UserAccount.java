package Models;

public class UserAccount {
    private String username;
    private String password;
    private String email;
    private String firstName;
    private String lastName;
    private boolean admin;

    public UserAccount(String username, String password, String firstName, String lastName, String email, boolean admin) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.admin = admin;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public boolean isAdmin() {
        return admin;
    }

    @Override
    public String toString() {
        return username + ","
        + password + ","
        + firstName + ","
        + lastName + ","
        + email + ","
        + String.valueOf(isAdmin());
    }
}