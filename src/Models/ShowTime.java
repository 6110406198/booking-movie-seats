package Models;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class ShowTime {
    private LocalDate showDate;
    private LocalTime showTime;
    private DateTimeFormatter dateFormat;
    private DateTimeFormatter timeFormat;

    public ShowTime(LocalDate showDate, LocalTime showTime) {
        this.showDate = showDate;
        this.showTime = showTime;
        this.dateFormat = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        this.timeFormat = DateTimeFormatter.ofPattern("HH:mm");
    }


    public LocalDate getShowDate() {
        return showDate;
    }

    public LocalTime getShowTime() {
        return showTime;
    }

    public String getShowTimeToString(){
        return showTime.format(timeFormat);
    }

    public String getShowDateToString(){
        return showDate.format(dateFormat);
    }



}
