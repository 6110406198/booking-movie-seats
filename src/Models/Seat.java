package Models;

public abstract class Seat {
    private String position;
    private boolean reserved;
    private double price;

    public Seat(String position){
        this.position = position;
        this.reserved = false;
    }

    public String getPosition(){
        return this.position;
    }

    public boolean isReserved() {
        return reserved;
    }

    public void setReserved(boolean reserved) {
        this.reserved = reserved;
    }

    public abstract double getPrice();

    public abstract void setPrice(double price);
}
