package Models;

public class SofaSeat extends Seat{
    private double price;
    public SofaSeat(String position) {
        super(position);
        this.price = 500;
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public void setPrice(double price) {
        this.price = price;
    }
}
