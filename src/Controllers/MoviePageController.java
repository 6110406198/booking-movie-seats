package Controllers;

import Models.Movie;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.text.Text;
import javafx.stage.Stage;


import java.io.IOException;
import java.net.URL;
import java.util.*;

public class MoviePageController implements Initializable {
    private List<Movie> movieList;
    private BookingController bookingController;
    private MovieController movieController;

    @FXML
    FlowPane movieFlow;
    @FXML
    ScrollPane scrollPane;
    @FXML
    List<Button> movieButton;



    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            bookingController = BookingController.getInstance();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            movieController = new MovieController();
        } catch (IOException e) {
            e.printStackTrace();
        }

        scrollPane.hbarPolicyProperty().setValue(ScrollPane.ScrollBarPolicy.NEVER);
        movieButton = new ArrayList<>();
        movieList = new ArrayList<>();
        Map<Button, Movie> btnMovie = new HashMap<>();

        try {
            movieList = movieController.getMovieList();
        } catch (IOException e) {
            e.printStackTrace();
        }


        for(Movie movie: movieList){
            ImageView temp  = new ImageView();
            temp.setImage(movie.getCover());
            temp.setPreserveRatio(true);
            temp.setFitHeight(240);

            Button tmpBtn = new Button();
            tmpBtn.setGraphic(temp); // set button image to movie poster
            tmpBtn.setFocusTraversable(false);
            btnMovie.put(tmpBtn, movie); // binding button with movie ตั้งปุ่มให้เป็นของหนังแต่ละเรื่อง
            tmpBtn.setOnAction(new EventHandler<ActionEvent>() { // test to binding OnClick function
                @Override
                public void handle(ActionEvent event) {
                    try {
                        bookingController.setMovie(btnMovie.get(tmpBtn));
                        changeToMovieDetailPage(event);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            });
            movieButton.add(tmpBtn);
        }


        movieFlow.setHgap(50);
        movieFlow.setVgap(50);
        movieFlow.getChildren().addAll(movieButton);

    }

    public void changeToMovieDetailPage(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../Views/MovieDetailPage.fxml"));
        stage.setScene(new Scene(loader.load(),800, 600));
        stage.show();
    }




}

