package Controllers;

import Models.UserAccount;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import com.sun.prism.paint.Color;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.paint.Paint;
import javafx.scene.text.Text;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;


public class SignUpController implements Initializable {
    @FXML
    private JFXTextField firstNameField, lastNameField, emailField, userNameField;
    @FXML
    private JFXPasswordField pwdField, repwdField;
    @FXML
    private JFXButton signUpBtn;
    @FXML
    private Text errFirst, errLast, errEmail, errUser, errPwd, resultText;
    private UserAccountController userAccountController;
    private String firstName, lastName, email, userName, pwd;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            userAccountController = new UserAccountController();
        } catch (IOException e) {
            e.printStackTrace();
        }

        signUpBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    handleSignUpButton(event);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

    }



    public void handleSignUpButton(ActionEvent event) throws IOException {
        checkForValidData();
        if(firstName != null && lastName != null && email != null && userName != null && pwd != null){
            UserAccount newUser = new UserAccount(userName, pwd,firstName,lastName,email,false);
            userAccountController.addNewUserAccount(newUser);
            resultText.setText("Sign In Sucessful !!!");
            resultText.setVisible(true);
            signUpBtn.setDisable(true);
        }
    }

    private void checkForValidData() throws IOException {
        handleFirstName();
        handleLastName();
        handleEmail();
        handleUserName();
        handlePassword();
    }

    private void handleFirstName(){
        if(isValidName(firstNameField.getText())){
            firstName = firstNameField.getText();
            errFirst.setVisible(false);
        }
        else if (firstNameField.getText().isEmpty()){
            setErrorMsg(errFirst, "Enter your First name");
        }
        else {
            setErrorMsg(errFirst,"It must be alphabet");
            firstNameField.setText("");
        }
    }

    private void handleLastName() {
        if(isValidName(lastNameField.getText())){
            lastName = lastNameField.getText();
            errLast.setVisible(false);
        }
        else if (lastNameField.getText().isEmpty()){
            setErrorMsg(errLast, "Enter your Last name");
        }
        else {
            setErrorMsg(errLast, "It must be alphabet");
            lastNameField.setText("");
        }
    }

    private void handleEmail(){
        if (isValidEmail(emailField.getText())){
            email = emailField.getText();
            errEmail.setVisible(false);
        }
        else if (emailField.getText().isEmpty()){
            setErrorMsg(errEmail, "Enter your E-mail");
        }
        else {
            setErrorMsg(errEmail, "Invalid E-mail");
            emailField.setText("");
        }
    }

    private void handleUserName() throws IOException {
        if (isValidUserName(userNameField.getText())){
            userName = userNameField.getText();
            errUser.setVisible(false);
        }
        else if (userNameField.getText().isEmpty()){
            setErrorMsg(errUser, "Enter your Username");
        }
        else if (userAccountController.getUserMap().containsKey(userNameField.getText())){
            setErrorMsg(errUser, "Can't use this ID");
            userNameField.setText("");
        }
        else {
            setErrorMsg(errUser, "Invalid Username");
            userNameField.setText("");
        }
    }

    private void handlePassword(){
        if (pwdField.getText().isEmpty()) {
            setErrorMsg(errPwd, "Enter your Password");
            repwdField.setText("");
        }
        else if (repwdField.getText().isEmpty()) setErrorMsg(errPwd, "Confirm your Password");
        else if (pwdField.getText().equals(repwdField.getText())){
            pwd = pwdField.getText();
            errPwd.setVisible(false);
        }
        else {
            setErrorMsg(errPwd, "It isn't match");
            pwdField.setText("");
            repwdField.setText("");
        }
    }

    private void setErrorMsg(Text errText, String msg){
        errText.setText(msg);
        errText.setFill(Paint.valueOf("RED"));
        errText.setVisible(true);
        pwdField.setText("");
        repwdField.setText("");

    }

    private boolean isValidName(String name){
        return name.matches("^[A-Za-z]+$");
    }

    private boolean isValidEmail(String email){
        return email.matches("^[A-Za-z][A-Za-z0-9.\\-_]*@[A-Za-z][A-Za-z0-9\\-]*\\.[a-z\\.]+$");
    }

    private boolean isValidUserName(String userName) throws IOException {
        Map<String, UserAccount> userAccountMap= userAccountController.getUserMap();
        return userName.matches("^[A-Za-z0-9\\-_]{4,}$") && !userAccountMap.containsKey(userName);
    }




}
