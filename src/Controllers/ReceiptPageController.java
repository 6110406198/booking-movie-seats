package Controllers;

import com.jfoenix.controls.JFXButton;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.stage.WindowEvent;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

public class ReceiptPageController implements Initializable {
    private BookingController bookingController;

    @FXML
    private ImageView poster;
    @FXML
    private Label movieName, showTime, theatreName, seatList, price;
    @FXML
    private JFXButton btn;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            bookingController = BookingController.getInstance();
        } catch (IOException e) {
            e.printStackTrace();
        }

        poster.setImage(bookingController.getMovie().getCover());
        movieName.setText(bookingController.getMovie().getName());
        showTime.setText(bookingController.getShowTime().getShowDateToString() + " | " + bookingController.getShowTime().getShowTimeToString());
        theatreName.setText(bookingController.getTheatre().getName());

        btn.setOnAction((event -> {
            try {
                handleConfirmDialog(event);
            } catch (IOException e) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("ERROR!!!");
                alert.setHeaderText("Can't load login page");
                alert.showAndWait();
            }
        }));

    }

    public void setTotalText(double num){
        price.setText(String.valueOf(num) + " ฿");
    }
    public void setListSeatText(List<String> list){
        seatList.setText(String.join(",",list));
    }

    private void handleConfirmDialog(ActionEvent event) throws IOException {
        bookingController.saveReservedSeat();
        JFXButton btn = (JFXButton) event.getSource();
        Stage receiptStage = (Stage) btn.getScene().getWindow();
        Stage bookingStage = (Stage) receiptStage.getOwner();

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Successful!!!");
        alert.setHeaderText("Booking Completed");
        alert.setContentText("Thank you. Do you want to continue booking?" + "\n\n");

        ButtonType booking = new ButtonType("Back to Booking");
        ButtonType logout = new ButtonType("Log out");
        ButtonType exit = new ButtonType("Exit Program");

        alert.getButtonTypes().setAll(booking, logout, exit);
        alert.initModality(Modality.WINDOW_MODAL);
        alert.initOwner(receiptStage);

        Optional<ButtonType> answer = alert.showAndWait();
        if(answer.get() == exit){
            System.exit(0);
        }
        else if (answer.get() == logout) {
            alert.close();
            receiptStage.close();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("../Views/LoginPage.fxml"));
            bookingStage.setScene(new Scene(loader.load()));
            bookingStage.show();
        }
        else if (answer.get() == booking){
            alert.close();
            receiptStage.close();
            String scene = "";
            String theatreName = bookingController.getTheatre().getName();
            if(theatreName.equals("Deluxe Hall")){
                scene = "../Views/DeluxeTheatrePage.fxml";
            }
            else if (theatreName.equals("Deluxe 4K Hall")){
                scene = "../Views/Deluxe4KTheatrePage.fxml";
            }
            else if (theatreName.equals("Supreme 4K Hall")){
                scene = "../Views/Supreme4KTheatrePage.fxml";
            }
            else if (theatreName.equals("3D Supreme Hall")){
                scene = "../Views/Supreme3DTheatrePage.fxml";
            }
            FXMLLoader loader = new FXMLLoader(getClass().getResource(scene));
            bookingStage.setScene(new Scene(loader.load()));
            bookingStage.show();


        }
    }

}
