package Controllers;

import Models.UserAccount;

import java.io.*;
import java.util.*;

public class UserAccountController {
    private final String FS = File.separator;
    private File userAccFile;
    private List<UserAccount> userAccounts;
    private Map<String, UserAccount> userMap;
    private StringBuilder userAccountString;

    public UserAccountController() throws IOException{
        userAccounts = new ArrayList<>();
        userMap = new HashMap<>();
        userAccountString = new StringBuilder();
        userAccFile = new File("data" + FS + "userAccount.csv");
        if (!userAccFile.exists()) userAccFile.createNewFile();
        else readUserAccount();
    }

    private void readUserAccount() throws IOException {
        if (userAccFile.length() > 0) {
            BufferedReader reader = new BufferedReader(new FileReader(userAccFile));
            String tmp = "";
            while ((tmp = reader.readLine()) != null) {
                String[] data = tmp.split(",");
                UserAccount user = new UserAccount(data[0], data[1], data[2], data[3], data[4], Boolean.parseBoolean(data[5].toLowerCase()));
                userAccountString.append(tmp);
                userAccountString.append(System.lineSeparator());
                userAccounts.add(user);
                userMap.put(user.getUsername(), user);
            }
            reader.close();
        }
    }

    public void addNewUserAccount(UserAccount newUser) throws IOException {
        userAccountString.append(newUser.toString());
        userAccountString.append(System.lineSeparator());

        BufferedWriter writer = new BufferedWriter(new FileWriter(userAccFile));
        writer.write(userAccountString.toString());
        writer.close();
    }

    public List<UserAccount> getUserAccounts() throws IOException {
        readUserAccount();
        return userAccounts;
    }

    public Map<String, UserAccount> getUserMap() throws IOException {
        readUserAccount();
        return userMap;
    }
}
