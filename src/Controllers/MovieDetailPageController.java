package Controllers;

import Models.Movie;
import Models.ShowTime;
import Models.Theatre;
import com.jfoenix.controls.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.chrono.ChronoLocalDateTime;
import java.util.ResourceBundle;
import java.util.regex.Pattern;

public class MovieDetailPageController implements Initializable {
    private BookingController bookingController;
    private String scene;

    @FXML
    private ImageView posterView;
    @FXML
    private JFXButton bookingBtn;
    @FXML
    private JFXDatePicker pickDate;
    @FXML
    private Label movieTitle, toBackPage;
    @FXML
    private Text detail;
    @FXML
    private JFXNodesList nodeBtn;
    @FXML
    private JFXButton firstNode;
    @FXML
    private JFXButton time1200btn;
    @FXML
    private JFXButton time1530btn;
    @FXML
    private JFXButton time1945btn;
    @FXML
    private JFXNodesList nodeTheatreBtn;
    @FXML
    private JFXButton firstNodeTheatre;
    @FXML
    private JFXButton theatreDeluxe;
    @FXML
    private JFXButton theaterDeluxe4K;
    @FXML
    private JFXButton supreme4K;
    @FXML
    private JFXButton supreme3D;



    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            bookingController = BookingController.getInstance();
        } catch (IOException e) {
            e.printStackTrace();
        }
        pickDate.setValue(LocalDate.now());

        nodeBtn.setSpacing(5);
        nodeTheatreBtn.setSpacing(0);

        posterView.setImage(bookingController.getMovie().getCover());
        movieTitle.setText(bookingController.getMovie().getName());
        detail.setText(bookingController.getMovie().getDescription());




    }

    public void checkTime(ActionEvent event){
        if (LocalTime.now().isAfter(LocalTime.of(12,00)) && pickDate.getValue().isEqual(LocalDate.now())){
            time1200btn.setDisable(true);
        }
        else time1200btn.setDisable(false);

        if (LocalTime.now().isAfter(LocalTime.of(15,30)) && pickDate.getValue().isEqual(LocalDate.now())){
            time1530btn.setDisable(true);
        }
        else time1530btn.setDisable(false);

        if (LocalTime.now().isAfter(LocalTime.of(19,45)) && pickDate.getValue().isEqual(LocalDate.now())){
            time1945btn.setDisable(true);
        }
        else time1945btn.setDisable(false);
    }

    public void toPreviousPage(MouseEvent event) throws IOException {
        Label b = (Label) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../Views/SelectMoviePage.fxml"));
        stage.setScene(new Scene(loader.load(),800, 600));
        stage.show();
    }

    public void handleDatePicker(){
        if (pickDate.getValue().isBefore(LocalDate.now())){
            pickDate.setValue(LocalDate.now());
        }

    }

    public void handleShowTimeButton(ActionEvent event){
        nodeBtn.animateList(false);
        JFXButton temp = (JFXButton) event.getSource();
        firstNode.setText(temp.getText());
    }

    public void handleTheatreButton(ActionEvent event){
        nodeTheatreBtn.animateList(false);
        JFXButton temp = (JFXButton) event.getSource();
        firstNodeTheatre.setText(temp.getText());
    }

    public void handleBookingButton(ActionEvent event){
         if (firstNode.getText().equals("Select ShowTime")){
            Alert warning = new Alert(Alert.AlertType.WARNING);
            warning.setTitle("Warning : Not found Show time");
            warning.setHeaderText("Please Select Show Time");
            warning.showAndWait();
         }
         else if (firstNodeTheatre.getText().equals("Select Theatre")){
            Alert warning = new Alert(Alert.AlertType.WARNING);
            warning.setTitle("Warning : Not found Theatre");
            warning.setHeaderText("Please Select Theatre");
            warning.showAndWait();
         }
         else {
            bookingController.setShowTime(new ShowTime(pickDate.getValue(), LocalTime.parse(firstNode.getText())));
            bookingController.setTheatre(new Theatre(firstNodeTheatre.getText()));

            String theatreName = bookingController.getTheatre().getName();
            if(theatreName.equals("Deluxe Hall")){
                scene = "../Views/DeluxeTheatrePage.fxml";
            }
            else if (theatreName.equals("Deluxe 4K Hall")){
                scene = "../Views/Deluxe4KTheatrePage.fxml";
            }
            else if (theatreName.equals("Supreme 4K Hall")){
                scene = "../Views/Supreme4KTheatrePage.fxml";
            }
            else if (theatreName.equals("3D Supreme Hall")){
                scene = "../Views/Supreme3DTheatrePage.fxml";
            }
            JFXButton b = (JFXButton) event.getSource();
            Stage stage = (Stage) b.getScene().getWindow();
            FXMLLoader loader = new FXMLLoader(getClass().getResource(scene));
            try {
                stage.setScene(new Scene(loader.load()));
            } catch (IOException e) {
                e.printStackTrace();
                System.err.println("Can't load BookingSeatPage");
            }
            stage.show();
        }

    }



}
