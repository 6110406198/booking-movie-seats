package Controllers;

import Models.UserAccount;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Paint;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

public class LoginController implements Initializable {
    private UserAccountController userAccountController;
    private BookingController bookingController;

    @FXML private JFXTextField idField;
    @FXML private JFXPasswordField pwdField;
    @FXML private JFXButton signIn;
    @FXML private Text signUp, errMsg;
    @FXML private Text aboutMeBtn;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            userAccountController = new UserAccountController();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            bookingController = BookingController.getInstance();
        } catch (IOException e) {
            e.printStackTrace();
        }
        errMsg.setVisible(false);
        //Sign In button
        signIn.setOnAction(e->{
            try {
                handleSignInButton(e);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });
        idField.getStyleClass().add("txtField");
        pwdField.getStyleClass().add("txtField");
        signIn.getStyleClass().add("signin");
        signUp.getStyleClass().add("textLink");
        aboutMeBtn.getStyleClass().add("textLink");
        signUp.setOnMouseClicked(this::handleSignUpButton);
    }// end of init

    public void handleSignInButton(ActionEvent event) throws IOException {
        Map<String, UserAccount> userAccountMap = userAccountController.getUserMap();
        String userId = idField.getText();
        String pwd = pwdField.getText();
        if (userAccountMap.isEmpty()) {
            errMsg.setText("ID isn't exist");
            errMsg.setVisible(true);
            idField.setText("");
            pwdField.setText("");
        } else {
            if (userId.length() < 4) {
                errMsg.setText("ID must has 4 or more characters");
                errMsg.setVisible(true);
                idField.setText("");
                pwdField.setText("");
            }
            if (!userId.matches("^[A-Za-z0-9\\-_]{4,}$")) {
                errMsg.setText("ID is invalid");
                errMsg.setVisible(true);
                idField.setText("");
                pwdField.setText("");
            }
            if (userAccountMap.containsKey(userId) && userAccountMap.get(userId).getPassword().equals(pwd)) {
                errMsg.setVisible(false);
                // go to select movie page
                bookingController.setUser(userAccountMap.get(userId));
                Button b = (Button) event.getSource();
                Stage stage = (Stage) b.getScene().getWindow();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("../Views/SelectMoviePage.fxml"));
                stage.setScene(new Scene(loader.load(), 800, 600));
                stage.show();
            } else {
                errMsg.setText("ID or Password is incorrect");
                errMsg.setVisible(true);
                pwdField.setText("");
            }
        }
    }


    public void handleSignUpButton(MouseEvent event){
        // when event interact with some component, Its will assign with event.getSource()
        Text txt = (Text) event.getSource();
        // set Stage with {{ above component }}.getScene().getWindow()
        // getWindow() in other word is get stage
        Stage parent = (Stage) txt.getScene().getWindow();
        Stage stage = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../Views/SignUpPage.fxml"));
        try {
            stage.setScene(new Scene(loader.load()));
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("Can't load Sign up page");
        }
        stage.setTitle("Sign up a new account");
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(parent);
        stage.showAndWait();

    }

    public void goToAboutMe(MouseEvent event) {
        Text txt = (Text) event.getSource();
        Stage stage = (Stage) txt.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../Views/AboutMe.fxml"));
        try {
            stage.setScene(new Scene(loader.load(), 800, 600));
            stage.getScene().getStylesheets().add("StyleSheet/LoginPageStyle.css");
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("ERROR!!!");
            alert.setHeaderText("Can't load About me page");
            alert.showAndWait();
        }
    }


}
