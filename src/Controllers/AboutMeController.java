package Controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;


import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;


public class AboutMeController implements Initializable {
    @FXML private ImageView imgView;
    @FXML private Text backBtn;

    public void handleBackBtn(MouseEvent event){
        Text txt = (Text) event.getSource();
        Stage stage = (Stage) txt.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../Views/LoginPage.fxml"));
        try {
            stage.setScene(new Scene(loader.load(), 800, 600));
            stage.show();
        } catch (IOException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("ERROR!!!");
            alert.setHeaderText("Can't load Login page");
            alert.showAndWait();
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        imgView.setImage(new Image("/image/me.png"));
        backBtn.getStyleClass().add("textLink");
    }
}
