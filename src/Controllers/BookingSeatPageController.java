package Controllers;

import Models.*;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.controls.JFXNodesList;
import com.jfoenix.transitions.hamburger.HamburgerBackArrowBasicTransition;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.shape.Shape;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.IOException;
import java.net.URL;
import java.util.*;

public class BookingSeatPageController implements Initializable {
    private BookingController bookingController;
    private Map<String, Seat> selectSeat;
    private double total;
    private Image unSelectNormal,selectedNormal,reservedNormal,
                  unSelectVip, selectedVip, reservedVip,
                  unSelectPremium, selectedPremium, reservedPremium;
    private List<String> reservedList;
    @FXML private GridPane gridHall;
    @FXML private Shape recMonitor;
    @FXML private Label labelLeftA, labelLeftB, labelLeftC, labelLeftD, labelLeftE,
                        labelRightA, labelRightB, labelRightC, labelRightD, labelRightE,
                        labelMonitor, priceLabel;

    @FXML private HBox  seatRowA, seatRowB, seatRowC, seatRowD, seatRowE;
    @FXML private List<HBox> seatRow;
    @FXML private ImageView A1, A2, A3, A4, A5, A6, A7, A8, A9, A10,
                            B1, B2, B3, B4, B5, B6, B7, B8, B9, B10,
                            C1, C2, C3, C4, C5, C6, C7, C8, C9, C10,
                            D1, D2, D3, D4, D5, D6, D7, D8, D9, D10,
                            E1, E2, E3, E4, E5, E6, E7, E8, E9, E10;
    @FXML private Text backTxt, logOut;
    @FXML private JFXButton bookingBtn;



    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            bookingController = BookingController.getInstance();
        } catch (IOException e) {
            e.printStackTrace();
        }
        reservedList = new ArrayList<>();
        seatRow = new ArrayList<>();
        selectSeat = new HashMap<>();
        loadImage();
        seatRow.add(seatRowA);
        seatRow.add(seatRowB);
        seatRow.add(seatRowC);
        seatRow.add(seatRowD);
        seatRow.add(seatRowE);

        priceLabel.setText("0.00 ฿");

        try {
            loadReservedSeat(bookingController.getTheatre().getName());
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (HBox row : seatRow) {
            row.getStylesheets().add("StyleSheet/BookingPageStyle.css");
            row.getStyleClass().add("seatRow");
            for (Node seat: row.getChildren()){
                seat.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        clickOnSeat(event);
                    }
                });
            }

        }

        logOut.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                Alert popup = new Alert(Alert.AlertType.CONFIRMATION);
                popup.setTitle("Logout Comfirmation");
                popup.setHeaderText("Do you want to logout?");
                popup.setContentText("Are you sure?");

                Optional<ButtonType> answer = popup.showAndWait();
                if (answer.get() == ButtonType.OK) {
                    Text txt = (Text) event.getSource();
                    Stage stage = (Stage) txt.getScene().getWindow();
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("../Views/LoginPage.fxml"));
                    try {
                        stage.setScene(new Scene(loader.load(), 800, 600));
                    } catch (IOException e) {
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.setTitle("ERROR!!!");
                        alert.setHeaderText("Can't load login page");
                        alert.showAndWait();
                    }
                    stage.show();
                }
            }
        });

        bookingBtn.setOnAction((event)->{
            try {
                handleBookingBtn(event);
            } catch (IOException e) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("ERROR!!!");
                alert.setHeaderText("Can't show receipt");
                alert.showAndWait();
                e.printStackTrace();
            }
        });


    } // end of initialize

    public void handlePriceLabel(){
        if (selectSeat.size() == 0){
            priceLabel.setText("0.00 ฿");
        }
        else{
            total = 0;
            for(Map.Entry<String, Seat> entry: selectSeat.entrySet()){
                total += entry.getValue().getPrice();
            }

            priceLabel.setText(total + " ฿");
        }
    }

    public void toPreviousPage(MouseEvent event) throws IOException {
        Text b = (Text) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../Views/SelectMoviePage.fxml"));
        stage.setScene(new Scene(loader.load(),800, 600));
        stage.show();
    }

    public void clickOnSeat(MouseEvent event){
        ImageView img = (ImageView) event.getSource();
        handleSelectSeat(img);
        handlePriceLabel();
    }

    private void handleSelectSeat(ImageView img) {
        Seat seat = bookingController.getTheatre().getSeatMap().get(img.getId());
        if (seat instanceof NormalSeat && !img.getImage().equals(reservedNormal)) {
                if (img.getImage().equals(unSelectNormal)) {
                    img.setImage(selectedNormal);
                    selectSeat.put(seat.getPosition(), seat);
                } else {
                    img.setImage(unSelectNormal);
                    selectSeat.remove(seat.getPosition());
                }
        } else if (seat instanceof VipSeat && !img.getImage().equals(reservedVip)) {
                if (img.getImage().equals(unSelectVip)) {
                    img.setImage(selectedVip);
                    selectSeat.put(seat.getPosition(), seat);
                } else {
                    img.setImage(unSelectVip);
                    selectSeat.remove(seat.getPosition());
                }
        } else if (seat instanceof SofaSeat && !img.getImage().equals(reservedPremium)) {
                if (img.getImage().equals(unSelectPremium)) {
                    img.setImage(selectedPremium);
                    selectSeat.put(seat.getPosition(), seat);
                } else {
                    img.setImage(unSelectPremium);
                    selectSeat.remove(seat.getPosition());
                }
        }
        else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("ERROR!!!");
            alert.setHeaderText("Can't select this seat.");
            alert.showAndWait();
        }
    }

    private void loadImage(){
        unSelectNormal = new Image("/image/Seat/armchair-unselect.png");
        selectedNormal = new Image("/image/Seat/armchair-selected.png");
        reservedNormal = new Image("/image/Seat/armchair-reserved.png");

        unSelectVip = new Image("/image/Seat/vipchair-unselect.png");
        selectedVip = new Image("/image/Seat/vipchair-selected.png");
        reservedVip = new Image("/image/Seat/vipchair-reserved.png");

        unSelectPremium = new Image("/image/Seat/premium-unselect.png");
        selectedPremium = new Image("/image/Seat/premium-selected.png");
        reservedPremium = new Image("/image/Seat/premium-reserved.png");
    }
    public void loadReservedSeat(String theatreName) throws IOException {
        reservedList = bookingController.getReservedList();
        if (theatreName.equals("Deluxe Hall")){
            for(HBox hbox: seatRow){
                for(Node seat: hbox.getChildren()){
                    bookingController.getTheatre().addSeat(new NormalSeat(seat.getId()));
                    if (reservedList.contains(seat.getId())){
                        ((ImageView) seat).setImage(reservedNormal);
                        bookingController.getTheatre().getSeatMap().get(seat.getId()).setReserved(true);
                    }
                    else {
                        ((ImageView) seat).setImage(unSelectNormal);
                    }
                }
            }
        }
        else if (theatreName.equals("Deluxe 4K Hall")){
            for (HBox hbox: seatRow){
                if(hbox.getId().equals("seatRowD") || hbox.getId().equals("seatRowE")){
                    for(Node seat: hbox.getChildren()){
                        bookingController.getTheatre().addSeat(new NormalSeat(seat.getId()));
                        if (reservedList.contains(seat.getId())){
                            ((ImageView) seat).setImage(reservedNormal);
                            bookingController.getTheatre().getSeatMap().get(seat.getId()).setReserved(true);
                        }
                        else {
                            ((ImageView) seat).setImage(unSelectNormal);
                        }
                    }
                }
                else{
                    for(Node seat: hbox.getChildren()){
                        bookingController.getTheatre().addSeat(new VipSeat(seat.getId()));
                        if (reservedList.contains(seat.getId())){
                            ((ImageView) seat).setImage(reservedVip);
                            bookingController.getTheatre().getSeatMap().get(seat.getId()).setReserved(true);
                        }
                        else {
                            ((ImageView) seat).setImage(unSelectVip);
                        }
                    }
                }

            }
        }
        else if (theatreName.equals("Supreme 4K Hall")){
            for(HBox hBox: seatRow){
                if (hBox.getId().equals("seatRowA")){
                    for(Node seat: hBox.getChildren()){
                        bookingController.getTheatre().addSeat(new SofaSeat(seat.getId()));
                        if (reservedList.contains(seat.getId())){
                            ((ImageView) seat).setImage(reservedPremium);
                            bookingController.getTheatre().getSeatMap().get(seat.getId()).setReserved(true);
                        }
                        else {
                            ((ImageView) seat).setImage(unSelectPremium);
                        }
                    }
                }
                else if (hBox.getId().equals("seatRowB") || hBox.getId().equals("seatRowC")){
                    for(Node seat: hBox.getChildren()){
                        bookingController.getTheatre().addSeat(new VipSeat(seat.getId()));
                        if (reservedList.contains(seat.getId())){
                            bookingController.getTheatre().getSeatMap().get(seat.getId()).setReserved(true);
                            ((ImageView) seat).setImage(reservedVip);
                        }
                        else {
                            ((ImageView) seat).setImage(unSelectVip);
                        }
                    }
                }
                else {
                    for (Node seat: hBox.getChildren()){
                        bookingController.getTheatre().addSeat(new NormalSeat(seat.getId()));
                        if (reservedList.contains(seat.getId())){
                            ((ImageView) seat).setImage(reservedNormal);
                            bookingController.getTheatre().getSeatMap().get(seat.getId()).setReserved(true);
                        }
                        else {
                            ((ImageView) seat).setImage(unSelectNormal);
                        }
                    }
                }
            }

        }
        else if (theatreName.equals("3D Supreme Hall")){
            for (HBox hbox: seatRow){
                if(hbox.getId().equals("seatRowA") || hbox.getId().equals("seatRowB")){
                    for(Node seat: hbox.getChildren()){
                        bookingController.getTheatre().addSeat(new SofaSeat(seat.getId()));
                        if (reservedList.contains(seat.getId())){
                            bookingController.getTheatre().getSeatMap().get(seat.getId()).setReserved(true);
                            ((ImageView) seat).setImage(reservedPremium);
                        }
                        else {
                            ((ImageView) seat).setImage(unSelectPremium);
                        }
                    }
                }
                else{
                    for(Node seat: hbox.getChildren()){
                        bookingController.getTheatre().addSeat(new VipSeat(seat.getId()));
                        if (reservedList.contains(seat.getId())){
                            bookingController.getTheatre().getSeatMap().get(seat.getId()).setReserved(true);
                            ((ImageView) seat).setImage(reservedVip);
                        }
                        else {
                            ((ImageView) seat).setImage(unSelectVip);
                        }
                    }
                }

            }
        }

    }

    private void handleBookingBtn(ActionEvent event) throws IOException {
        if (selectSeat.size() != 0) {
            for(Map.Entry<String, Seat> entry: selectSeat.entrySet()){
                bookingController.addSeat(entry.getKey());
            }
            JFXButton btn = (JFXButton) event.getSource();
            Stage parent = (Stage) btn.getScene().getWindow();
            Stage stage = new Stage();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("../Views/ReceiptPage.fxml"));
            stage.setScene(new Scene(loader.load()));
            stage.setTitle("Confirm Booking Seat");
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(parent);

            ReceiptPageController receiptPageController = loader.getController();
            receiptPageController.setTotalText(this.total);
            receiptPageController.setListSeatText(getSelectList());

            stage.showAndWait();
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("ERROR!!!");
            alert.setHeaderText("Please select one or more seat.");
            alert.showAndWait();
        }
    }

    public List<String> getSelectList(){
        List<String> list = new ArrayList<>();
        for(Map.Entry<String, Seat> entry: selectSeat.entrySet()){
            list.add(entry.getKey());
        }
        Collections.sort(list);
        return list;
    }

    public double getTotal(){
        return total;
    }








} // end of class


