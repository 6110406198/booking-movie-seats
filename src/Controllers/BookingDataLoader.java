package Controllers;

import Models.*;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/* username, movie, showtime, theatre, List<seat>
    data[0] = username
    data[1] = movie name
    data[2] = showdate
    data[3] = showtime
    data[4] = theatre
    data[5] - data[data.length -1] = reserved seat
 */


public class BookingDataLoader {
    private final String FS = File.separator;
    private File bookingFile;
    private List<String> reservedSeatList;

    public BookingDataLoader() throws IOException {
        reservedSeatList = new ArrayList<>();
        initBookingFile();
    }

    private void initBookingFile() throws IOException {
        bookingFile = new File("data" + FS + "BookingData.csv");
        if (!bookingFile.exists()) bookingFile.createNewFile();
    }



    public List<String> getReservedSeatList(Theatre theatre, Movie movie, ShowTime showTime) throws IOException{
        if (!reservedSeatList.isEmpty()) reservedSeatList.clear();
        BufferedReader reader = new BufferedReader(new FileReader(bookingFile));
        String tmp = "";
        while ((tmp = reader.readLine()) != null) {
            String[] data = tmp.split(",");
            if(data[1].equals(movie.getName())
            && data[2].equals(showTime.getShowDateToString())
            && data[3].equals(showTime.getShowTimeToString())
            && data[4].equals(theatre.getName())
            ){
                if (data.length > 5) {
                    for (int i = 5; i < data.length; i++){
                        reservedSeatList.add(data[i]);
                    }
                }
            }
        }
        reader.close();

        return reservedSeatList;
    }

    public void saveReservedSeat(String username, String movieName, String showDate, String showTime, String theatreName, List<String> list) throws IOException {
        PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(bookingFile, true)));
        String data = username+","+movieName+","+showDate+","+showTime+","+theatreName+",";
        writer.println(data + String.join(",", list));
        writer.flush();
        writer.close();

    }
}
