package Controllers;

import Models.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class BookingController {
    private static BookingController instance;

    private UserAccount user;
    private Movie movie;
    private ShowTime showTime;
    private Theatre theatre;
    private List<String> bookingSeat;

    private BookingDataLoader bookingDataLoader;

    private BookingController() throws IOException {
        bookingSeat = new ArrayList<>();
        bookingDataLoader = new BookingDataLoader();
    }

    public static BookingController getInstance() throws IOException {
        if (instance == null){
            instance = new BookingController();
        }
        return instance;
    }

    public List<String> getReservedList() throws IOException {
        return bookingDataLoader.getReservedSeatList(this.theatre, this.movie, this.showTime);
    }

    public void saveReservedSeat() throws IOException {
        bookingDataLoader.saveReservedSeat(user.getUsername(), movie.getName(), showTime.getShowDateToString(), showTime.getShowTimeToString(), theatre.getName(), bookingSeat);
        bookingSeat.clear();
    }

    public void setUser(UserAccount user) {
        this.user = user;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public void setShowTime(ShowTime showTime) {
        this.showTime = showTime;
    }

    public void setTheatre(Theatre theatre) {
        this.theatre = theatre;
    }

    public void addSeat(List<String> list){
        bookingSeat.addAll(list);
    }

    public void addSeat(String seatPosition){
        bookingSeat.add(seatPosition);
    }

    public UserAccount getUser() {
        return user;
    }

    public Movie getMovie() {
        return movie;
    }

    public ShowTime getShowTime() {
        return showTime;
    }

    public Theatre getTheatre() {
        return theatre;
    }
}
