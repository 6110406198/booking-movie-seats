package Controllers;


import Models.Movie;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class MovieController {
    private final String FS = File.separator;
    private File movieFile;
    private List<Movie> movieList;
    private StringBuilder movieText;

    public MovieController() throws IOException {
        movieList = new ArrayList<>();
        movieText = new StringBuilder();
        movieFile = new File("data" + FS + "movieList.txt");
        if (!movieFile.exists()) movieFile.createNewFile();
        else {
            readMovie();
        }
    }

    private void readMovie() throws IOException {
        if (movieFile.length() > 0 ){
            BufferedReader reader = new BufferedReader(new FileReader(movieFile));
            String tmp = "";
            while ((tmp = reader.readLine()) != null) {
                String[] data = tmp.split("\\|");
                Movie movie = new Movie(data[0], data[1]);
                movie.setDescription(data[2]);
                movieText.append(tmp);
                movieText.append(System.lineSeparator());
                movieList.add(movie);
            }
            reader.close();
        }

    }


    public List<Movie> getMovieList() throws IOException {
        movieList.clear();
        readMovie();
        return movieList;
    }

}
